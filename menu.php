<div class="quixnav">
    <div class="quixnav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label first">Main Menu</li>
            <li><a href="index.php"><i class="icon icon-home"></i><span class="nav-text">Dashboard</span></a></li>
            <li><a href="kelola_sales.php"><i class="icon icon-single-04"></i><span class="nav-text">Kelola Sales</span></a></li>
            <li><a href="kelola_kriteria.php"><i class="icon icon-single-copies"></i><span class="nav-text">Kelola Kriteria</span></a></li>
            <li><a href="kelola_alternatif.php"><i class="icon icon-menu-left"></i><span class="nav-text">Kelola Alternatif</span></a></li>
            <li><a href="perbandingan_kriteria.php"><i class="icon icon-chart-bar-33"></i><span class="nav-text">Perbandingan Kriteria</span></a></li>
            <li><a href="perbandingan_alternatif.php"><i class="icon icon-analytics"></i><span class="nav-text">Perbandingan Alternatif</span></a></li>
            <li><a href="laporan.php"><i class="icon icon-e-reader"></i><span class="nav-text">Laporan</span></a></li>



<!--             
<li><a href="index.html"><i class="icon icon-cloud-download-95"></i><span class="nav-text">icon-cloud-download-95</span></a></li>
<li><a href="index.html"><i class="icon icon-home-minimal"></i><span class="nav-text">icon-home-minimal</span></a></li>
<li><a href="index.html"><i class="icon icon-single-04"></i><span class="nav-text">icon-single-04</span></a></li>
<li><a href="index.html"><i class="icon icon-users-mm"></i><span class="nav-text">icon-users-mm</span></a></li>
<li><a href="index.html"><i class="icon icon-webpage"></i><span class="nav-text">icon-webpage</span></a></li>
<li><a href="index.html"><i class="icon icon-layout-25"></i><span class="nav-text">icon-layout-25</span></a></li>
<li><a href="index.html"><i class="icon icon-analytics"></i><span class="nav-text">icon-analytics</span></a></li>
<li><a href="index.html"><i class="icon icon-chart-pie-36"></i><span class="nav-text">icon-chart-pie-36</span></a></li>
<li><a href="index.html"><i class="icon icon-chart-bar-33"></i><span class="nav-text">icon-chart-bar-33</span></a></li>
<li><a href="index.html"><i class="icon icon-single-copy-06"></i><span class="nav-text">icon-single-copy-06</span></a></li>
<li><a href="index.html"><i class="icon icon-home"></i><span class="nav-text">icon-home</span></a></li>
<li><a href="index.html"><i class="icon icon-single-content-03"></i><span class="nav-text">icon-single-content-03</span></a></li>
<li><a href="index.html"><i class="icon icon-bell-53"></i><span class="nav-text">icon-bell-53</span></a></li>
<li><a href="index.html"><i class="icon icon-email-84"></i><span class="nav-text">icon-email-84</span></a></li>
<li><a href="index.html"><i class="icon icon-send"></i><span class="nav-text">icon-send</span></a></li>
<li><a href="index.html"><i class="icon icon-at-sign"></i><span class="nav-text">icon-at-sign</span></a></li>
<li><a href="index.html"><i class="icon icon-attach-87"></i><span class="nav-text">icon-attach-87</span></a></li>
<li><a href="index.html"><i class="icon icon-edit-72"></i><span class="nav-text">icon-edit-72</span></a></li>
<li><a href="index.html"><i class="icon icon-tail-right"></i><span class="nav-text">icon-tail-right</span></a></li>
<li><a href="index.html"><i class="icon icon-minimal-right"></i><span class="nav-text">icon-minimal-right</span></a></li>
<li><a href="index.html"><i class="icon icon-tail-left"></i><span class="nav-text">icon-tail-left</span></a></li>
<li><a href="index.html"><i class="icon icon-minimal-left"></i><span class="nav-text">icon-minimal-left</span></a></li>
<li><a href="index.html"><i class="icon icon-tail-up"></i><span class="nav-text">icon-tail-up</span></a></li>
<li><a href="index.html"><i class="icon icon-minimal-up"></i><span class="nav-text">icon-minimal-up</span></a></li>
<li><a href="index.html"><i class="icon icon-minimal-down"></i><span class="nav-text">icon-minimal-down</span></a></li>
<li><a href="index.html"><i class="icon icon-tail-down"></i><span class="nav-text">icon-tail-down</span></a></li>
<li><a href="index.html"><i class="icon icon-settings-gear-64"></i><span class="nav-text">icon-settings-gear-64</span></a></li>
<li><a href="index.html"><i class="icon icon-settings"></i><span class="nav-text">icon-settings</span></a></li>
<li><a href="index.html"><i class="icon icon-menu-dots"></i><span class="nav-text">icon-menu-dots</span></a></li>
<li><a href="index.html"><i class="icon icon-menu-left"></i><span class="nav-text">icon-menu-left</span></a></li>
<li><a href="index.html"><i class="icon icon-funnel-40"></i><span class="nav-text">icon-funnel-40</span></a></li>
<li><a href="index.html"><i class="icon icon-filter"></i><span class="nav-text">icon-filter</span></a></li>
<li><a href="index.html"><i class="icon icon-preferences-circle"></i><span class="nav-text">icon-preferences-circle</span></a></li>
<li><a href="index.html"><i class="icon icon-check-2"></i><span class="nav-text">icon-check-2</span></a></li>
<li><a href="index.html"><i class="icon icon-cart-simple"></i><span class="nav-text">icon-cart-simple</span></a></li>
<li><a href="index.html"><i class="icon icon-cart-9"></i><span class="nav-text">icon-cart-9</span></a></li>
<li><a href="index.html"><i class="icon icon-card-update"></i><span class="nav-text">icon-card-update</span></a></li>
<li><a href="index.html"><i class="icon icon-basket"></i><span class="nav-text">icon-basket</span></a></li>
<li><a href="index.html"><i class="icon icon-check-circle-07"></i><span class="nav-text">icon-check-circle-07</span></a></li>
<li><a href="index.html"><i class="icon icon-simple-remove"></i><span class="nav-text">icon-simple-remove</span></a></li>
<li><a href="index.html"><i class="icon icon-circle-remove"></i><span class="nav-text">icon-circle-remove</span></a></li>
<li><a href="index.html"><i class="icon icon-alert-circle-exc"></i><span class="nav-text">icon-alert-circle-exc</span></a></li>
<li><a href="index.html"><i class="icon icon-bug"></i><span class="nav-text">icon-bug</span></a></li>
<li><a href="index.html"><i class="icon icon-share-66"></i><span class="nav-text">icon-share-66</span></a></li>
<li><a href="index.html"><i class="icon icon-time-3"></i><span class="nav-text">icon-time-3</span></a></li>
<li><a href="index.html"><i class="icon icon-time"></i><span class="nav-text">icon-time</span></a></li>
<li><a href="index.html"><i class="icon icon-coffee"></i><span class="nav-text">icon-coffee</span></a></li>
<li><a href="index.html"><i class="icon icon-smile"></i><span class="nav-text">icon-smile</span></a></li>
<li><a href="index.html"><i class="icon icon-sad"></i><span class="nav-text">icon-sad</span></a></li>
<li><a href="index.html"><i class="icon icon-broken-heart"></i><span class="nav-text">icon-broken-heart</span></a></li>
<li><a href="index.html"><i class="icon icon-heart-2"></i><span class="nav-text">icon-heart-2</span></a></li>
<li><a href="index.html"><i class="icon icon-pin-3"></i><span class="nav-text">icon-pin-3</span></a></li>
<li><a href="index.html"><i class="icon icon-marker-3"></i><span class="nav-text">icon-marker-3</span></a></li>
<li><a href="index.html"><i class="icon icon-globe-2"></i><span class="nav-text">icon-globe-2</span></a></li>
<li><a href="index.html"><i class="icon icon-world-2"></i><span class="nav-text">icon-world-2</span></a></li>
<li><a href="index.html"><i class="icon icon-phone-2"></i><span class="nav-text">icon-phone-2</span></a></li>
<li><a href="index.html"><i class="icon icon-check-square-11"></i><span class="nav-text">icon-check-square-11</span></a></li>
<li><a href="index.html"><i class="icon icon-wallet-90"></i><span class="nav-text">icon-wallet-90</span></a></li>
<li><a href="index.html"><i class="icon icon-credit-card"></i><span class="nav-text">icon-credit-card</span></a></li>
<li><a href="index.html"><i class="icon icon-payment"></i><span class="nav-text">icon-payment</span></a></li>
<li><a href="index.html"><i class="icon icon-tag"></i><span class="nav-text">icon-tag</span></a></li>
<li><a href="index.html"><i class="icon icon-tag-cut"></i><span class="nav-text">icon-tag-cut</span></a></li>
<li><a href="index.html"><i class="icon icon-tag-content"></i><span class="nav-text">icon-tag-content</span></a></li>
<li><a href="index.html"><i class="icon icon-flag-diagonal-33"></i><span class="nav-text">icon-flag-diagonal-33</span></a></li>
<li><a href="index.html"><i class="icon icon-triangle-right-17"></i><span class="nav-text">icon-triangle-right-17</span></a></li>
<li><a href="index.html"><i class="icon icon-puzzle-10"></i><span class="nav-text">icon-puzzle-10</span></a></li>
<li><a href="index.html"><i class="icon icon-triangle-right-17-2"></i><span class="nav-text">icon-triangle-right-17-2</span></a></li>
<li><a href="index.html"><i class="icon icon-btn-play"></i><span class="nav-text">icon-btn-play</span></a></li>
<li><a href="index.html"><i class="icon icon-btn-play-2"></i><span class="nav-text">icon-btn-play-2</span></a></li>
<li><a href="index.html"><i class="icon icon-menu-34"></i><span class="nav-text">icon-menu-34</span></a></li>
<li><a href="index.html"><i class="icon icon-menu-left-2"></i><span class="nav-text">icon-menu-left-2</span></a></li>
<li><a href="index.html"><i class="icon icon-heart-2-2"></i><span class="nav-text">icon-heart-2-2</span></a></li>
<li><a href="index.html"><i class="icon icon-single-04-2"></i><span class="nav-text">icon-single-04-2</span></a></li>
<li><a href="index.html"><i class="icon icon-users-mm-2"></i><span class="nav-text">icon-users-mm-2</span></a></li>
<li><a href="index.html"><i class="icon icon-l-settings"></i><span class="nav-text">icon-l-settings</span></a></li>
<li><a href="index.html"><i class="icon icon-book-open-2"></i><span class="nav-text">icon-book-open-2</span></a></li>
<li><a href="index.html"><i class="icon icon-layers-3"></i><span class="nav-text">icon-layers-3</span></a></li>
<li><a href="index.html"><i class="icon icon-logo-fb-simple"></i><span class="nav-text">icon-logo-fb-simple</span></a></li>
<li><a href="index.html"><i class="icon icon-logo-twitter"></i><span class="nav-text">icon-logo-twitter</span></a></li>
<li><a href="index.html"><i class="icon icon-google"></i><span class="nav-text">icon-google</span></a></li>
<li><a href="index.html"><i class="icon icon-logo-pinterest"></i><span class="nav-text">icon-logo-pinterest</span></a></li>
<li><a href="index.html"><i class="icon icon-logo-instagram"></i><span class="nav-text">icon-logo-instagram</span></a></li>
<li><a href="index.html"><i class="icon icon-logo-dribbble"></i><span class="nav-text">icon-logo-dribbble</span></a></li>
<li><a href="index.html"><i class="icon icon-tablet-mobile"></i><span class="nav-text">icon-tablet-mobile</span></a></li>
<li><a href="index.html"><i class="icon icon-house-search-engine"></i><span class="nav-text">icon-house-search-engine</span></a></li>
<li><a href="index.html"><i class="icon icon-house-pricing"></i><span class="nav-text">icon-house-pricing</span></a></li>
<li><a href="index.html"><i class="icon icon-pulse-chart"></i><span class="nav-text">icon-pulse-chart</span></a></li>
<li><a href="index.html"><i class="icon icon-plug"></i><span class="nav-text">icon-plug</span></a></li>
<li><a href="index.html"><i class="icon icon-app-store"></i><span class="nav-text">icon-app-store</span></a></li>
<li><a href="index.html"><i class="icon icon-power-level"></i><span class="nav-text">icon-power-level</span></a></li>
<li><a href="index.html"><i class="icon icon-window-add"></i><span class="nav-text">icon-window-add</span></a></li>
<li><a href="index.html"><i class="icon icon-form"></i><span class="nav-text">icon-form</span></a></li>
<li><a href="index.html"><i class="icon icon-folder-15"></i><span class="nav-text">icon-folder-15</span></a></li>
<li><a href="index.html"><i class="icon icon-lock"></i><span class="nav-text">icon-lock</span></a></li>
<li><a href="index.html"><i class="icon icon-unlocked"></i><span class="nav-text">icon-unlocked</span></a></li>
<li><a href="index.html"><i class="icon icon-e-reader"></i><span class="nav-text">icon-e-reader</span></a></li>
<li><a href="index.html"><i class="icon icon-layout-grid"></i><span class="nav-text">icon-layout-grid</span></a></li>
<li><a href="index.html"><i class="icon icon-single-copies"></i><span class="nav-text">icon-single-copies</span></a></li> -->


            
        </ul>
    </div>
</div>