<?php
    session_start();
    if(!isset($_SESSION["username"])){
        header("Location: login.php");
    }

    $servername = "localhost";
    $username = "root";
    // $password = "mysql";
    $password = "";
    $db = "AHP_Sales";

    // Create connection
    $conn = mysqli_connect($servername, $username, $password,$db);

    // Check connection
    if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }
?>