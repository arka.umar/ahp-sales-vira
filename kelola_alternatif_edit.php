<!DOCTYPE html>
<html lang="en">

<head>
    <title>Kelola Alternatif - Ubah Data</title>
    <?php include("header.php") ?>
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">
        <?php  include("header-nav.php") ?>
        <?php  include("menu.php") ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <h4>Ubah Data Alternatif</h4>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="kelola_alternatif.php">Kelola Alternatif</a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Ubah Data</a></li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="basic-form">
                                    <form id="form_create">
                                        <?php 
                                            $sql = "SELECT * FROM alternatif where id=".$_GET["id"];
                                            $result = $conn->query($sql);
                                            $id = "";
                                            $nama_alternatif = "";
                                            if ($result->num_rows > 0) {
                                                while($row = $result->fetch_assoc()) {
                                                    $id = $row["id"];
                                                    $nama_alternatif = $row["nama_alternatif"];
                                                }
                                            }
                                            
                                        ?>

                                        <input type="hidden" name="id" value ="<?php echo $id?>">
                                        <div class="form-group col-md-6">
                                            <label class="label">Nama Alternatif <span style="color:red;">*</span> : </label>
                                            <input type="text" id="nama_alternatif" name="nama_alternatif" value ="<?php echo $nama_alternatif?>" class="form-control input-default " maxlength="100" required autocomplete="off">
                                        </div>
                                    </form>
                                </div>
                                <div class="pull-right">
                                    <button type="button" id="btnCancel" class="btn btn-light">Batal</button> &nbsp; 
                                    <button type="button" id="btnSave" class="btn btn-dark">Ubah</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <?php include("footer.php") ?>
        
    </div>
    <!-- Required vendors -->
    <?php include("loadscript.php") ?>

    <script>

        (function($) {
            "use strict"

            $("#btnCancel").on('click', function(){
                window.location.replace("kelola_alternatif.php");
            })

            $("#btnSave").on('click', function(){
                if($("#form_create").valid()){
                    $.ajax({
                        type: "POST",
                        data: $('#form_create').serialize() + "&action=kelola_alternatif_update",
                        url: "action.php",
                        timeout: 9000, //in milliseconds
                        success: function(data){
                            if(data.rcode == "SUCCESS"){
                                toastr.success(data.msg);
                                setTimeout(function(){
                                    window.location.replace("kelola_alternatif.php");
                                }, 1500);
                            } else {
                                console.log(data);
                                toastr.error(data.msg);
                            }
                        },
                        error: function(request, status, err){
                            console.log(request);
                            toastr.error(status + " " + err);
                        },
                    });
                } 
            });

        })(jQuery);

        var input = document.getElementById("nama_alternatif");
        input.addEventListener("keydown", function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                document.getElementById("btnSave").click();
            }
        });


    </script>
        
    </script>
</body>

</html>