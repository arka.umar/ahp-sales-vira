<!DOCTYPE html>
<html lang="en">

<head>
    <title>Perbandingan Alternatif </title>
    <?php include("header.php") ?>
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">
        <?php  include("header-nav.php") ?>
        <?php  include("menu.php") ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <h4>Perbandingan Alternatif</h4>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="perbandingan_alternatif.php">Perbandingan Alternatif</a></li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <form id="form_perbandingan">
                                    <?php 
                                        $sql = "SELECT a.id, a.nama_kriteria
                                                FROM kriteria a
                                                ORDER BY a.id
                                        ";
                                        $resultcriteria = $conn->query($sql);
                                        $critno = 0;
                                        if ($resultcriteria->num_rows > 0) {
                                            while($rowcriteria = $resultcriteria->fetch_assoc()) {
                                                $critno += 1;

                                    ?>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <h4>Perbandingan Alternatif Kriteria : <?php echo $rowcriteria["nama_kriteria"]?></h4>
                                            <div class="table-responsive">
                                                <table id="tabelalternatif" class="table table-bordered" style="">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2">Pilih Alternatif Yang Lebih Peniting</th>
                                                            <th>Nilai Perbandingan</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                            $sql = "SELECT
                                                                        a.id,
                                                                        a.nama_alternatif ,
                                                                        b.id as id2,
                                                                        b.nama_alternatif as nama_alternatif2 
                                                                    FROM
                                                                        alternatif a CROSS JOIN alternatif b 
                                                                    WHERE
                                                                        a.id != b.id and a.id < b.id
                                                                    ORDER BY
                                                                        a.id, b.id
                                                            ";
                                                            $result = $conn->query($sql);
                                                            $no = 0;
                                                            if ($result->num_rows > 0) {
                                                                while($row = $result->fetch_assoc()) {
                                                                    $no = $no+1;
                                                                    echo "
                                                                        <tr>
                                                                            <td>".$row["nama_alternatif"]."</td>
                                                                            <td>".$row["nama_alternatif2"]."</td>
                                                                            <td>
                                                                                <input type='number' name='matrix~".$row["id"]."~".$row["id2"]."~".$rowcriteria["id"]."' class='form-control input-default' maxlength='100' required autocomplete='off'>
                                                                            </td>
                                                                        </tr>
                                                                    ";
                                                                }
                                                            }
                                                            
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <?php if($critno == 1){?>
                                                <h4>&nbsp;</h4>
                                                <div class="table-responsive">
                                                    <table id="tabelalternatif" class="table table-bordered" style="">
                                                        <thead>
                                                            <tr>
                                                                <th style="width:1px;">Nilai</th>
                                                                <th>Keterangan</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr><td>1</td><td>Kedua alternatif sama penting</td></tr>
                                                            <tr><td>3</td><td>Alternatif yang satu seidikit lebih penting</td></tr>
                                                            <tr><td>5</td><td>Alternatif yang satu seidikit lebih penting alternatif daripada yang lainnya</td></tr>
                                                            <tr><td>7</td><td>Alternatif yang satu jelas sangat penting daripada alternatif yang lainnya</td></tr>
                                                            <tr><td>9</td><td>Alternatif yang satu mutlak sangat penting daripada alternatif yang lainnya</td></tr>
                                                            <tr><td>2,4,6,8</td><td>Nilai tengah diantara dua pertimbangan yang berdekatan</td></tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>

                                    <?php 
                                            }
                                        }
                                    ?>


                                    <div class="row">
                                        <div class="col-md-6">
                                            <button type="button" id="btnSave" class="btn btn-dark">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <?php include("footer.php") ?>
        
    </div>

    <!-- Required vendors -->
    <?php include("loadscript.php") ?>
    
    <script>

        (function($) {
            "use strict"


            $("#btnSave").on('click', function(){
                if($("#form_perbandingan").valid()){
                    $.ajax({
                        type: "POST",
                        data: $('#form_perbandingan').serialize() + "&action=perbandingan_alternatif_save",
                        url: "action.php",
                        timeout: 9000, //in milliseconds
                        success: function(data){
                            if(data.rcode == "SUCCESS"){
                                toastr.success(data.msg);
                                setTimeout(function(){
                                    window.location.replace("perbandingan_alternatif_matrix.php");
                                }, 1500);
                            } else {
                                console.log(data);
                                toastr.error(data.msg);
                            }
                        },
                        error: function(request, status, err){
                            console.log(request);
                            toastr.error(status + " " + err);
                        },
                    });
                } 
            });

        })(jQuery);

    </script>

    
</body>

</html>