<!DOCTYPE html>
<html lang="en">

<head>
    <title>Perbandingan Kriteria - Matrix Berpasangan </title>
    <?php include("header.php") ?>
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">
        <?php  include("header-nav.php") ?>
        <?php  include("menu.php") ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <h4>Matrix Berpasangan Kriteria </h4>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="perbandingan_kriteria.php">Perbandingan Kriteria</a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Matrix Berpasangan</a></li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5>Matrix Perbandingan Berpasangan</h5>
                                        <div class="table-responsive">
                                                <table id="tabelkriteria" class="table table-bordered" style="">
                                                    <thead>
                                                        <tr>
                                                            <th>Kriteria</th>
                                                            <?php 
                                                                $sql = "SELECT a.id, a.nama_kriteria
                                                                        FROM kriteria a
                                                                        ORDER BY a.id
                                                                ";
                                                                $result = $conn->query($sql);
                                                                if ($result->num_rows > 0) {
                                                                    while($row = $result->fetch_assoc()) {
                                                                        echo "<th>".$row["nama_kriteria"]."</th>";
                                                                    }
                                                                }
                                                            ?>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                            $sql = "SELECT
                                                                        a.id,
                                                                        a.nama_kriteria,
                                                                        GROUP_CONCAT(b.nilai_perbandingan) as nilai_perbandingan,
                                                                        SUM(nilai_perbandingan) as SUM_nilai_perbandingan
                                                                    FROM
                                                                        kriteria a
                                                                        INNER JOIN (
                                                                            SELECT t.*, COALESCE(c.nilai_perbandingan, 1) as nilai_perbandingan
                                                                            FROM ( SELECT a.id, b.id as id2 FROM kriteria a CROSS JOIN kriteria b) t
                                                                            LEFT JOIN matrix_perbandingan_kriteria c ON (t.id = c.id AND t.id2 = c.id2) OR (t.id = c.id2 AND t.id2 = c.id)
                                                                        ) b ON a.id = b.id
                                                                    GROUP BY a.id
                                                                    ORDER BY a.id
                                                            ";
                                                            $result = $conn->query($sql);

                                                            if ($result->num_rows > 0) {
                                                                $num_row = $result->num_rows;
                                                                $jumlah = array();
                                                                while($row = $result->fetch_assoc()) {
                                                                    $no = $no+1;
                                                                    echo "<tr><td>".$row["nama_kriteria"]."</td>";

                                                                    for($x=1; $x <= $num_row; $x++){
                                                                        $nilai = (int)explode(",",$row["nilai_perbandingan"])[$x-1];
                                                                        if(!isset($jumlah[$x-1])) $jumlah[$x-1] = 0;
                                                                        if($no > $x){
                                                                            echo "<td>". number_format(1/$nilai,"3") ."</td>";
                                                                            $jumlah[$x-1] += (1/$nilai);
                                                                        } else {
                                                                            echo "<td>". number_format($nilai,"3") ."</td>";
                                                                            $jumlah[$x-1] += $nilai;
                                                                        }
                                                                    }
                                                                    echo "</tr>";
                                                                }
                                                                
                                                                echo "<tr><td>Jumlah</td>";
                                                                for($x=1; $x <= $no; $x++){
                                                                    echo "<td>".number_format($jumlah[$x-1],"3")."</td>";
                                                                }
                                                                echo "</tr>";

                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-top:30px;">
                                    <div class="col-md-12">
                                        <h5>Matrix Nilai Kriteria</h5>
                                        <div class="table-responsive">
                                                <table id="tabelkriteria" class="table table-bordered" style="">
                                                    <thead>
                                                        <tr>
                                                            <th>Kriteria</th>
                                                            <?php 
                                                                $sql = "SELECT a.id, a.nama_kriteria
                                                                        FROM kriteria a
                                                                        ORDER BY a.id
                                                                ";
                                                                $result = $conn->query($sql);
                                                                if ($result->num_rows > 0) {
                                                                    while($row = $result->fetch_assoc()) {
                                                                        echo "<th>".$row["nama_kriteria"]."</th>";
                                                                    }
                                                                }
                                                                
                                                            ?>
                                                            <th>Total</th>
                                                            <th>Rata - Rata</th>
                                                            <th>Weight SUM Vector</th>
                                                            <th>Consistency Vector</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                            $sql = "SELECT
                                                                        a.id,
                                                                        a.nama_kriteria,
                                                                        GROUP_CONCAT(b.nilai_perbandingan) as nilai_perbandingan,
                                                                        SUM(nilai_perbandingan) as SUM_nilai_perbandingan
                                                                    FROM
                                                                        kriteria a
                                                                        INNER JOIN (
                                                                            SELECT t.*, COALESCE(c.nilai_perbandingan, 1) as nilai_perbandingan
                                                                            FROM ( SELECT a.id, b.id as id2 FROM kriteria a CROSS JOIN kriteria b) t
                                                                            LEFT JOIN matrix_perbandingan_kriteria c ON (t.id = c.id AND t.id2 = c.id2) OR (t.id = c.id2 AND t.id2 = c.id)
                                                                        ) b ON a.id = b.id
                                                                    GROUP BY a.id
                                                                    ORDER BY a.id
                                                            ";
                                                            $result = $conn->query($sql);

                                                            if ($result->num_rows > 0) {
                                                                $num_row = $result->num_rows;
                                                                $no = 0;
                                                                $kriteria_id = array();
                                                                $rata = array();
                                                                $totalmatrix = array();
                                                                while($row = $result->fetch_assoc()) {
                                                                    $priority_vector = 0;
                                                                    $no = $no+1;
                                                                    $rata[$no-1] = 0;
                                                                    $totalmatrix[$no-1] = 0;
                                                                    $kriteria_id[$no-1] = $row["id"];
                                                                    // echo "<tr><td>".$row["nama_kriteria"]."</td>";
                                                                    for($x=1; $x <= $num_row; $x++){
                                                                        $nilai = (int)explode(",",$row["nilai_perbandingan"])[$x-1];

                                                                        if($no > $x){
                                                                            $nilai = (1/$nilai)/$jumlah[$x-1];
                                                                        } else {
                                                                            $nilai = ($nilai)/$jumlah[$x-1];
                                                                        }
                                                                        // echo "<td>". number_format($nilai,"3") ."</td>";
                                                                        $totalmatrix[$no-1] += $nilai;
                                                                    }

                                                                    $rata[$no-1] = $totalmatrix[$no-1]/$num_row;
                                                                    // echo "<td>".number_format($total,"3")."</td>";
                                                                    // echo "<td>". number_format($rata[$no-1],"3") ."</td>";
                                                                    // echo "<td>". number_format($priority_vector,"3") ."</td>";
                                                                    // echo "<td id='cv-".($no-1)."'></td>";
                                                                    // echo "</tr>";

                                                                }
                                                            }

                                                            $sql = "SELECT
                                                                        a.id,
                                                                        a.nama_kriteria,
                                                                        GROUP_CONCAT(b.nilai_perbandingan) as nilai_perbandingan,
                                                                        SUM(nilai_perbandingan) as SUM_nilai_perbandingan
                                                                    FROM
                                                                        kriteria a
                                                                        INNER JOIN (
                                                                            SELECT t.*, COALESCE(c.nilai_perbandingan, 1) as nilai_perbandingan
                                                                            FROM ( SELECT a.id, b.id as id2 FROM kriteria a CROSS JOIN kriteria b) t
                                                                            LEFT JOIN matrix_perbandingan_kriteria c ON (t.id = c.id AND t.id2 = c.id2) OR (t.id = c.id2 AND t.id2 = c.id)
                                                                        ) b ON a.id = b.id
                                                                    GROUP BY a.id
                                                                    ORDER BY a.id
                                                            ";
                                                            $result = $conn->query($sql);

                                                            $totalCV = 0;
                                                            if ($result->num_rows > 0) {
                                                                $num_row = $result->num_rows;
                                                                $no = 0;
                                                                while($row = $result->fetch_assoc()) {
                                                                    $priority_vector = 0;
                                                                    $no = $no+1;
                                                                    echo "<tr><td>".$row["nama_kriteria"]."</td>";
                                                                    $total = 0;
                                                                    for($x=1; $x <= $num_row; $x++){
                                                                        $nilai = (int)explode(",",$row["nilai_perbandingan"])[$x-1];
                                                                        // if($priority_vector < ($nilai/$total)) $priority_vector = $nilai/$total;

                                                                        if($no > $x){
                                                                            $nilai = (1/$nilai);
                                                                        } else {
                                                                            $nilai = ($nilai);
                                                                        }
                                                                        echo "<td>". number_format(($nilai/$jumlah[$x-1]),"3") ."</td>";
                                                                        $total += ($nilai*$rata[$x-1]);
                                                                    }
                                                                    
                                                                    echo "<td>".number_format($totalmatrix[$no-1],3)."</td>";
                                                                    echo "<td>".number_format($rata[$no-1],3)."</td>";
                                                                    echo "<td>".number_format($total,3)."</td>";
                                                                    echo "<td>".number_format(($total/$rata[$no-1]),3)."</td>";
                                                                    
                                                                    $totalCV += ($total/$rata[$no-1]);

                                                                    echo "</tr>";

                                                                }
                                                            }
                                                        ?>
                                                        <?php
                                                            $pmax = ($totalCV/$num_row);
                                                            $ci = ($pmax-$num_row)/($num_row-1);
                                                            $cr = $ci/0.9;
                                                        ?>
                                                        <tr>
                                                            <td colspan="<?php echo $num_row+4 ?>">λ Max</td>
                                                            <td><?php echo number_format($pmax,3) ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="<?php echo $num_row+4 ?>">Consistency Index</td>
                                                            <td><?php echo number_format($ci,3) ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="<?php echo $num_row+4 ?>">Consistency Ratio</td>
                                                            <td><?php echo number_format($cr,3) ?></td>
                                                        </tr>
                                                        <?php
                                                            // dipake di db
                                                            // $rata untuk bobot
                                                            // 
                                                            $postRata = array();
                                                            for($x=1; $x <= $num_row; $x++){
                                                                $object = array();
                                                                $object["id"] = $kriteria_id[$x-1];
                                                                $object["rata"] = $rata[$x-1];
                                                                $postRata [] = $object;
                                                            }

                                                            // print_r($postRata);
                                                            echo "<input type='hidden' id='formRata' value='".json_encode($postRata)."' autocomplete='off'>"

                                                        ?>
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <!-- <button type="button" id="btnSave" class="btn btn-dark">Submit</button> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <?php include("footer.php") ?>
        
    </div>

    <!-- Required vendors -->
    <?php include("loadscript.php") ?>
    
    <script>

        (function($) {
            "use strict"


            $("#btnSave").on('click', function(){
                if($("#form_perbandingan").valid()){
                    $.ajax({
                        type: "POST",
                        data: $('#form_perbandingan').serialize() + "&action=perbandingan_kriteria_save",
                        url: "action.php",
                        timeout: 9000, //in milliseconds
                        success: function(data){
                            if(data.rcode == "SUCCESS"){
                                toastr.success(data.msg);
                                setTimeout(function(){
                                    window.location.replace("kelola_kriteria.php");
                                }, 1500);
                            } else {
                                console.log(data);
                                toastr.error(data.msg);
                            }
                        },
                        error: function(request, status, err){
                            console.log(request);
                            toastr.error(status + " " + err);
                        },
                    });
                } 
            });

            setTimeout(function(){
                if($("#formRata").val() != ''){
                    var data = new Array();
                    data.push({ name: "action", value: "perbandingan_kriteria_save_rata" } );
                    data.push({ name: "formRata", value: $("#formRata").val() });

                    $.ajax({
                        type: "POST",
                        data: data,
                        url: "action.php",
                        timeout: 9000, //in milliseconds
                        success: function(data){
                            if(data.rcode == "SUCCESS"){
                                console.log(data);
                            } else {
                                console.log(data);
                                toastr.error(data.msg);
                            }
                        },
                        error: function(request, status, err){
                            console.log(request);
                            toastr.error(status + " " + err);
                        },
                    });
                } 

            }, 500);

        })(jQuery);

    </script>

    
</body>

</html>