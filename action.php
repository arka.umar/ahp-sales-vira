<?php
	header('Content-Type: application/json');

    $servername = "localhost";
    $username = "root";
    // $password = "mysql";
    $password = "";
    $db = "AHP_Sales";

    $conn = mysqli_connect($servername, $username, $password,$db);

	$res = array();
    if (!$conn) {
		$res['rcode'] = "ERROR";
		$res['data'] = $_POST;
		$res['msg'] = "Connection failed: " . mysqli_connect_error();
		echo json_encode($res);
    } else {
        if (isset($_POST['action'])){
            $action = strtolower($_POST['action']);

            // SALES
            //// hapus sales
            if($action == "kelola_sales_delete"){
                if(!isset($_POST['id'])) {
                    $res['rcode'] = "ERROR";
                    $res['msg'] = "id harus diisi!";
                    echo json_encode($res);
                    return;
                }
                
                $sql = "delete from sales where id=".$_POST['id'];
                $result = $conn->query($sql);

                $res['rcode'] = "SUCCESS";
                $res['msg'] = "Data berhasil dihapus.";
                echo json_encode($res);

            //// simpan sales
            } elseif($action == "kelola_sales_save"){
                // $sql = "INSERT INTO sales(nama_sales, no_tim, penempatan) values('".$_POST['nama_sales']."',".$_POST['no_tim'].",'".$_POST['penempatan']."')";
                $sql = "INSERT INTO sales(nama_sales, no_tim, penempatan) values('".$_POST['nama_sales']."',".$_POST['no_tim'].",'')";
                $result = $conn->query($sql);
                if($result === TRUE){
                    $res['rcode'] = "SUCCESS";
                    $res['msg'] = "Data berhasil disimpan.";
                } else {
                    $res['rcode'] = "ERROR";
                    $res['msg'] = "Data gagal disimpan!<br/>".$conn->error;
                }
                $res['data'] = $_POST;
                echo json_encode($res);

            //// simpan sales
            } elseif($action == "kelola_sales_update"){
                // $sql = "UPDATE sales set nama_sales='".$_POST['nama_sales']."' , no_tim=".$_POST['no_tim'].", penempatan='".$_POST['penempatan']."' where id=".$_POST['id'];
                $sql = "UPDATE sales set nama_sales='".$_POST['nama_sales']."' , no_tim=".$_POST['no_tim']." where id=".$_POST['id'];
                $result = $conn->query($sql);
                if($result === TRUE){
                    $res['rcode'] = "SUCCESS";
                    $res['msg'] = "Data berhasil diubah.";
                } else {
                    $res['rcode'] = "ERROR";
                    $res['msg'] = "Data gagal diubah!<br/>".$conn->error;
                }
                $res['data'] = $_POST;
                echo json_encode($res);



            // KRITERIA
            //// hapus kriteria
            } elseif($action == "kelola_kriteria_delete"){
                if(!isset($_POST['id'])) {
                    $res['rcode'] = "ERROR";
                    $res['msg'] = "id harus diisi!";
                    echo json_encode($res);
                    return;
                }
                
                $sql = "delete from kriteria where id=".$_POST['id'];
                $result = $conn->query($sql);

                $res['rcode'] = "SUCCESS";
                $res['msg'] = "Data berhasil dihapus.";
                echo json_encode($res);

            //// simpan kriteria
            } elseif($action == "kelola_kriteria_save"){
                $sql = "INSERT INTO kriteria(nama_kriteria) values('".$_POST['nama_kriteria']."')";
                $result = $conn->query($sql);
                if($result === TRUE){
                    $res['rcode'] = "SUCCESS";
                    $res['msg'] = "Data berhasil disimpan.";
                } else {
                    $res['rcode'] = "ERROR";
                    $res['msg'] = "Data gagal disimpan!<br/>".$conn->error;
                }
                $res['data'] = $_POST;
                echo json_encode($res);

            //// simpan kriteria
            } elseif($action == "kelola_kriteria_update"){
                $sql = "UPDATE kriteria set nama_kriteria='".$_POST['nama_kriteria']."' where id=".$_POST['id'];
                $result = $conn->query($sql);
                if($result === TRUE){
                    $res['rcode'] = "SUCCESS";
                    $res['msg'] = "Data berhasil diubah.";
                } else {
                    $res['rcode'] = "ERROR";
                    $res['msg'] = "Data gagal diubah!<br/>".$conn->error;
                }
                $res['data'] = $_POST;
                echo json_encode($res);



            // ALTERNATIF
            //// hapus alternatif
            } elseif($action == "kelola_alternatif_delete"){
                if(!isset($_POST['id'])) {
                    $res['rcode'] = "ERROR";
                    $res['msg'] = "id harus diisi!";
                    echo json_encode($res);
                    return;
                }
                
                $sql = "delete from alternatif where id=".$_POST['id'];
                $result = $conn->query($sql);

                $res['rcode'] = "SUCCESS";
                $res['msg'] = "Data berhasil dihapus.";
                echo json_encode($res);

            //// simpan alternatif
            } elseif($action == "kelola_alternatif_save"){
                $sql = "INSERT INTO alternatif(nama_alternatif) values('".$_POST['nama_alternatif']."')";
                $result = $conn->query($sql);
                if($result === TRUE){
                    $res['rcode'] = "SUCCESS";
                    $res['msg'] = "Data berhasil disimpan.";
                } else {
                    $res['rcode'] = "ERROR";
                    $res['msg'] = "Data gagal disimpan!<br/>".$conn->error;
                }
                $res['data'] = $_POST;
                echo json_encode($res);

            //// simpan alternatif
            } elseif($action == "kelola_alternatif_update"){
                $sql = "UPDATE alternatif set nama_alternatif='".$_POST['nama_alternatif']."' where id=".$_POST['id'];
                $result = $conn->query($sql);
                if($result === TRUE){
                    $res['rcode'] = "SUCCESS";
                    $res['msg'] = "Data berhasil diubah.";
                } else {
                    $res['rcode'] = "ERROR";
                    $res['msg'] = "Data gagal diubah!<br/>".$conn->error;
                }
                $res['data'] = $_POST;
                echo json_encode($res);
                



            // PERBANDINGAN KRITERIA
            //// save perbandingan kriteria
            } elseif($action == "perbandingan_kriteria_save"){
                $sql = "";
                foreach($_POST as $key => $value)
                {
                    if (strstr($key, 'matrix~'))
                    {
                        $sql = "INSERT INTO matrix_perbandingan_kriteria VALUES('".explode("~",$key)[1]."','".explode("~",$key)[2]."','".$value."') ON DUPLICATE KEY UPDATE nilai_perbandingan=".$value;
                        $result = $conn->query($sql);
                    }
                }

                if($result === TRUE){
                    $res['rcode'] = "SUCCESS";
                    $res['msg'] = "Data berhasil disimpan.";
                } else {
                    $res['rcode'] = "ERROR";
                    $res['msg'] = "Data gagal disimpan!<br/>".$conn->error;
                }
                $res['data'] = $_POST;
                echo json_encode($res);
            } elseif($action == "perbandingan_kriteria_save_rata"){
                $sql = "";
                $dataRata = json_decode($_POST['formRata']);
                
                foreach($dataRata as $r){
                    $sql = "UPDATE kriteria set bobot = '".$r->rata."' where id = '".$r->id."'";
                    $result = $conn->query($sql);
                }

                $res['rcode'] = "SUCCESS";
                $res['msg'] = "Data berhasil disimpan.";
                $res['data'] = $_POST;
                echo json_encode($res);


            // PERBANDINGAN ALTERMATIF
            //// save perbandingan alternatif
            } elseif($action == "perbandingan_alternatif_save"){
                $sql = "";
                foreach($_POST as $key => $value)
                {
                    if (strstr($key, 'matrix~'))
                    {
                        $sql = "INSERT INTO matrix_perbandingan_alternatif VALUES('".explode("~",$key)[3]."','".explode("~",$key)[1]."','".explode("~",$key)[2]."','".$value."') ON DUPLICATE KEY UPDATE nilai_perbandingan=".$value;
                        $result = $conn->query($sql);
                    }
                }

                if($result === TRUE){
                    $res['rcode'] = "SUCCESS";
                    $res['msg'] = "Data berhasil disimpan.";
                } else {
                    $res['rcode'] = "ERROR";
                    $res['msg'] = "Data gagal disimpan!<br/>".$conn->error;
                }
                $res['data'] = $_POST;
                echo json_encode($res);
            } elseif($action == "perbandingan_alternatif_save_rata"){
                $sql = "";
                $dataRata = json_decode($_POST['formRata']);
                
                foreach($dataRata as $r){
                    $sql = "INSERT INTO bobot values ('".$r->id."', '".$r->id_kriteria."', '".$r->rata."') ON DUPLICATE KEY UPDATE bobot = '".$r->rata."'";
                    $result = $conn->query($sql);
                }

                $res['rcode'] = "SUCCESS";
                $res['msg'] = "Data berhasil disimpan.";
                $res['data'] = $_POST;
                echo json_encode($res);



            // LAPORAN
            //// setTim
            } elseif($action == "update_tim"){
                $sql = "UPDATE sales set penempatan='' where penempatan='".$_POST['nama_alternatif']."'";
                $result2 = $conn->query($sql);
                $sql = "UPDATE sales set penempatan='".$_POST['nama_alternatif']."' where no_tim='".$_POST['no_tim']."'";
                $result = $conn->query($sql);
                if($result === TRUE){
                    $res['rcode'] = "SUCCESS";
                    $res['msg'] = "Tim berhasil di set.";
                } else {
                    $res['rcode'] = "ERROR";
                    $res['msg'] = "Tim gagal di set!<br/>".$conn->error;
                }
                $res['data'] = $_POST;
                echo json_encode($res);




            } else {
                $res['rcode'] = "ERROR";
                $res['msg'] = "can't find action!";
                echo json_encode($res);
            }
    
        } else {
            $res['rcode'] = "ERROR";
            $res['data'] = $_POST;
            $res['msg'] = "can't find action!";
            echo json_encode($res);
        }
    }

?>