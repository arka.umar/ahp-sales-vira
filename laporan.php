<!DOCTYPE html>
<html lang="en">

<head>
    <title>Laporan</title>
    <?php include("header.php") ?>
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">
        <?php  include("header-nav.php") ?>
        <?php  include("menu.php") ?>
<style>
    .select2-results__option{
        color:black !important;
    }
</style>
        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <h4>Laporan </h4>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active"><a href="laporan.php">Laporan</a></li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body" id="printArea">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h5>Laporan Hasil Perhitungan</h5>
                                        <div class="table-responsive">
                                                <table id="tabelLaporan" class="table table-bordered" style="">
                                                    <thead>
                                                        <tr>
                                                            <th>Kriteria</th>
                                                            <th>Bobot</th>
                                                            <?php 
                                                                $jumlah_alternatif = 0;
                                                                $sql = "SELECT a.id, a.nama_alternatif
                                                                        FROM alternatif a
                                                                        ORDER BY a.id
                                                                ";
                                                                $result = $conn->query($sql);
                                                                $list_alternatif = array();
                                                                if ($result->num_rows > 0) {
                                                                    $jumlah_alternatif = $result->num_rows;
                                                                    while($row = $result->fetch_assoc()) {
                                                                        $list_alternatif[] = $row["id"];
                                                                        echo "<th>".$row["nama_alternatif"]."</th>";
                                                                    }
                                                                }
                                                            ?>

                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                            $sql = "SELECT
                                                                        a.*,
                                                                        GROUP_CONCAT(b.bobot ORDER BY b.id_alternatif SEPARATOR '~' ) as bobot_alternatif
                                                                    FROM
                                                                        kriteria a
                                                                        left join bobot b on a.id = b.id_kriteria
                                                                    GROUP BY a.id
                                                                    ORDER BY a.id
                                                            ";
                                                            $result = $conn->query($sql);

                                                            if ($result->num_rows > 0) {
                                                                $num_row = $result->num_rows;
                                                                $no = 0;
                                                                $total = array();
                                                                while($row = $result->fetch_assoc()) {
                                                                    $no = $no+1;
                                                                    $bobot = $row["bobot"];
                                                                    echo "<tr><td>".$row["nama_kriteria"]."</td><td>".number_format($row["bobot"],3)."</td>";

                                                                    for($x=1; $x <= $jumlah_alternatif; $x++){
                                                                        $nilai = explode("~",$row["bobot_alternatif"])[$x-1];
                                                                        $total[$x-1] += $bobot*$nilai;
                                                                        echo "<td>". number_format($nilai,"3") ."</td>";
                                                                    }
                                                                    echo "</tr>";
                                                                }
                                                                
                                                                echo "<tr><td colspan='2'>TOTAL</td>";
                                                                for($x=1; $x <= $jumlah_alternatif; $x++){
                                                                    echo "<td>".number_format($total[$x-1],"3")."</td>";
                                                                }
                                                                echo "</tr>";

                                                                // find rangking
                                                                $rangking = array();
                                                                $last = 0;
                                                                for($x=1; $x <= $jumlah_alternatif; $x++){
                                                                    $object = array();
                                                                    $object["id"] = $list_alternatif[$x-1];
                                                                    $object["total"] = $total[$x-1];
                                                                    if($total[$x-1] > $last){
                                                                        $rangking[] = $object;
                                                                        $last = $total[$x-1];
                                                                    } else {
                                                                        $temp = array();
                                                                        $temp = array_merge($rangking,$temp);
                                                                        $rangking = array();
                                                                        $rangking[] = $object;
                                                                        $rangking = array_merge($rangking,$temp);
                                                                    }
                                                                    
                                                                }
                                                                // arsort($rangking);
                                                                // print_r($rangking);
    

                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" style="margin-top:30px;">
                                    <div class="col-lg-6">
                                        <h5>Tabel Rangking Hasil Perhitungan</h5>
                                        <div class="table-responsive">
                                                <table id="tabelRangking" class="table table-bordered" style="">
                                                    <thead>
                                                        <tr>
                                                            <th>Nama Alternatif</th>
                                                            <th>Skor Akhir</th>
                                                            <th>Rangking</th>
                                                            <th>No Team</th>
                                                            <th>Sales</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                            $sql2 = "select no_tim,GROUP_CONCAT( DISTINCT nama_sales)  as nama_sales from sales group by no_tim order by no_tim";
                                                            $result = $conn->query($sql2);
                                                            $listNoTim = array();
                                                            if ($result->num_rows > 0) {
                                                                while($row = $result->fetch_assoc()) {
                                                                    $listNoTim[] = $row["no_tim"]."~".$row["nama_sales"];
                                                                }
                                                            }


                                                            $sql = "
                                                                SELECT
                                                                    a.id,
                                                                    a.nama_alternatif,
                                                                    max(b.no_tim) as no_tim,
                                                                    GROUP_CONCAT( DISTINCT b.nama_sales)  as nama_sales
                                                                FROM
                                                                    alternatif a 
                                                                    left join sales b ON a.nama_alternatif = b.penempatan
                                                                GROUP BY
                                                                    a.id
                                                                ORDER BY
                                                                    a.id
                                                            ";
                                                            $result = $conn->query($sql);
                                                            if ($result->num_rows > 0) {
                                                                $no = 0;
                                                                while($row = $result->fetch_assoc()) {
                                                                    $no += 1;
                                                                    echo "
                                                                        <tr>
                                                                            <td class='cityName'>".$row["nama_alternatif"]."</td>
                                                                            <td>".number_format($total[$no-1],3)."</td>
                                                                    ";

                                                                    for($x=1; $x <= $jumlah_alternatif; $x++){
                                                                        if($rangking[$x-1]["id"] == $row["id"]){
                                                                            echo "<td>". ($jumlah_alternatif-$x+1)."</td>";
                                                                        }
                                                                    }


                                                                    echo '<td><select class="form-control select2team" id="notim-'.$row["id"].'" data-id="'.$row["id"].'">';
                                                                    echo '<option value="">Pilih Tim</option>';
                                                                    foreach($listNoTim as $val){
                                                                        $selected = "";
                                                                        $val_no_tim = explode('~', $val)[0];
                                                                        $val_nama_sales = explode('~', $val)[1];
                                                                        if($row["no_tim"] == $val_no_tim) $selected = " selected='selected'";

                                                                        echo '<option value="'.$val_no_tim.'" data-name="'.$val_nama_sales.'" '.$selected.'>'.$val_no_tim.'</option>';
                                                                    }
                                                                    echo '</select></td><td class="nama_sales">'.$row["nama_sales"].'</td></tr>';
                                                                }
                                                            }
                                                        ?>


                                                    </tbody>
                                                    
                                                </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row" id="hide-print">
                                    <div class="col-md-6">
                                        <!-- <button type="button" id="btnSave" class="btn btn-dark">Submit</button> -->
                                        <!-- <input class="btn btn-primary" type="button" onclick="printDiv('printArea')" value=" Cetak " /> -->
                                        <input class="btn btn-primary" type="button" onclick="printDiv('printArea')" value=" Cetak " />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <?php include("footer.php") ?>
        
    </div>

    <!-- Required vendors -->
    <?php include("loadscript.php") ?>
    
    <script>

        (function($) {
            "use strict"


            $("#btnSave").on('click', function(){
                if($("#form_perbandingan").valid()){
                    $.ajax({
                        type: "POST",
                        data: $('#form_perbandingan').serialize() + "&action=perbandingan_kriteria_save",
                        url: "action.php",
                        timeout: 9000, //in milliseconds
                        success: function(data){
                            if(data.rcode == "SUCCESS"){
                                toastr.success(data.msg);
                                setTimeout(function(){
                                    window.location.replace("kelola_kriteria.php");
                                }, 1500);
                            } else {
                                console.log(data);
                                toastr.error(data.msg);
                            }
                        },
                        error: function(request, status, err){
                            console.log(request);
                            toastr.error(status + " " + err);
                        },
                    });
                } 
            });

            $(".select2team").select2({
            }).on("select2:select", function(e) {
                var id_alternatif = $(e.currentTarget).attr('data-id');
                var nama_alternatif = $(e.currentTarget).closest('tr').find('.cityName').html();
                var no_tim = e.params.data.id;
                var nama_sales = $(e.currentTarget).find(':selected').data('name');
                $(e.currentTarget).closest('tr').find('.nama_sales').html(nama_sales);
                // console.log(id_alternatif)
                // console.log(no_tim)
                // console.log(nama_alternatif)

                if(no_tim == '') {
                    $(e.currentTarget).closest('tr').find('.nama_sales').html('');
                    var data = new Array();
                    data.push({ name: "action", value: "update_tim" } );
                    data.push({ name: "id_alternatif", value: id_alternatif });
                    data.push({ name: "nama_alternatif", value: nama_alternatif });
                    data.push({ name: "no_tim", value: no_tim });
                    console.log(data)

                    $.ajax({
                        type: "POST",
                        data: data,
                        url: "action.php",
                        timeout: 9000, //in milliseconds
                        success: function(data){
                            if(data.rcode == "SUCCESS"){
                                toastr.success(data.msg);
                            } else {
                                console.log(data);
                                toastr.error(data.msg);
                            }
                        },
                        error: function(request, status, err){
                            console.log(request);
                            toastr.error(status + " " + err);
                        },
                    });

                } else {
                    $('#tabelRangking > tbody > tr').each(function(){
                        if($(this).find('.select2team').attr('data-id') != id_alternatif){
                            if($(this).find('.select2team').val() == no_tim){
                                $(this).find('.select2team').val('').trigger('change');
                                $(this).find('.nama_sales').html('');
                            }
                        }
                    });


                    var data = new Array();
                    data.push({ name: "action", value: "update_tim" } );
                    data.push({ name: "id_alternatif", value: id_alternatif });
                    data.push({ name: "nama_alternatif", value: nama_alternatif });
                    data.push({ name: "no_tim", value: no_tim });

                    $.ajax({
                        type: "POST",
                        data: data,
                        url: "action.php",
                        timeout: 9000, //in milliseconds
                        success: function(data){
                            if(data.rcode == "SUCCESS"){
                                toastr.success(data.msg);
                            } else {
                                console.log(data);
                                toastr.error(data.msg);
                            }
                        },
                        error: function(request, status, err){
                            console.log(request);
                            toastr.error(status + " " + err);
                        },
                    });
                }

            });

        })(jQuery);

        function printDiv(divName) {
            // window.open("laporan_cetak.php", '_blank');

            // var popup = window.open("laporan_cetak.php", "Popup", "left=10000,top=10000,width=0,height=0");
            // setTimeout(() => {
            //     popup.close();
            // }, 1000);
            var x = document.getElementById("hide-print");
            x.style.display = "none";

            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;


            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
            var x = document.getElementById("hide-print");
            x.style.display = "block";
        }

    </script>

    
</body>

</html>