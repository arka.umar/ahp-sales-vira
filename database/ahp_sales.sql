/*
 Navicat Premium Data Transfer

 Source Server         : mysql localhost
 Source Server Type    : MySQL
 Source Server Version : 80018
 Source Host           : localhost:3306
 Source Schema         : ahp_sales

 Target Server Type    : MySQL
 Target Server Version : 80018
 File Encoding         : 65001

 Date: 29/08/2021 20:10:50
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alternatif
-- ----------------------------
DROP TABLE IF EXISTS `alternatif`;
CREATE TABLE `alternatif`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_alternatif` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of alternatif
-- ----------------------------
INSERT INTO `alternatif` VALUES (1, 'Sukabumi');
INSERT INTO `alternatif` VALUES (2, 'Tasik Malaya');
INSERT INTO `alternatif` VALUES (3, 'Cirebon');

-- ----------------------------
-- Table structure for bobot
-- ----------------------------
DROP TABLE IF EXISTS `bobot`;
CREATE TABLE `bobot`  (
  `id_alternatif` int(11) NOT NULL,
  `id_kriteria` int(11) NOT NULL,
  `bobot` double NOT NULL,
  PRIMARY KEY (`id_alternatif`, `id_kriteria`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of bobot
-- ----------------------------
INSERT INTO `bobot` VALUES (1, 1, 0.38730158730159);
INSERT INTO `bobot` VALUES (1, 2, 0.63982683982684);
INSERT INTO `bobot` VALUES (1, 3, 0.33333333333333);
INSERT INTO `bobot` VALUES (1, 5, 0.33333333333333);
INSERT INTO `bobot` VALUES (2, 1, 0.44285714285714);
INSERT INTO `bobot` VALUES (2, 2, 0.82164502164502);
INSERT INTO `bobot` VALUES (2, 3, 0.33333333333333);
INSERT INTO `bobot` VALUES (2, 5, 0.33333333333333);
INSERT INTO `bobot` VALUES (3, 1, 0.16984126984127);
INSERT INTO `bobot` VALUES (3, 2, 0.2961038961039);
INSERT INTO `bobot` VALUES (3, 3, 0.33333333333333);
INSERT INTO `bobot` VALUES (3, 5, 0.33333333333333);

-- ----------------------------
-- Table structure for kriteria
-- ----------------------------
DROP TABLE IF EXISTS `kriteria`;
CREATE TABLE `kriteria`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kriteria` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `bobot` double NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kriteria
-- ----------------------------
INSERT INTO `kriteria` VALUES (1, 'Track Record Penjualan', 0.375);
INSERT INTO `kriteria` VALUES (2, 'Modal', 0.125);
INSERT INTO `kriteria` VALUES (3, 'UMR', 0.125);
INSERT INTO `kriteria` VALUES (5, 'Jarak', 0.375);

-- ----------------------------
-- Table structure for matrix_perbandingan_alternatif
-- ----------------------------
DROP TABLE IF EXISTS `matrix_perbandingan_alternatif`;
CREATE TABLE `matrix_perbandingan_alternatif`  (
  `id_kriteria` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `id2` int(11) NOT NULL,
  `nilai_perbandingan` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_kriteria`, `id`, `id2`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of matrix_perbandingan_alternatif
-- ----------------------------
INSERT INTO `matrix_perbandingan_alternatif` VALUES (1, 1, 2, 1);
INSERT INTO `matrix_perbandingan_alternatif` VALUES (1, 1, 3, 2);
INSERT INTO `matrix_perbandingan_alternatif` VALUES (1, 2, 3, 3);
INSERT INTO `matrix_perbandingan_alternatif` VALUES (2, 1, 2, 1);
INSERT INTO `matrix_perbandingan_alternatif` VALUES (2, 1, 3, 2);
INSERT INTO `matrix_perbandingan_alternatif` VALUES (2, 2, 3, 3);
INSERT INTO `matrix_perbandingan_alternatif` VALUES (3, 1, 2, 1);
INSERT INTO `matrix_perbandingan_alternatif` VALUES (3, 1, 3, 1);
INSERT INTO `matrix_perbandingan_alternatif` VALUES (3, 2, 3, 1);
INSERT INTO `matrix_perbandingan_alternatif` VALUES (5, 1, 2, 1);
INSERT INTO `matrix_perbandingan_alternatif` VALUES (5, 1, 3, 1);
INSERT INTO `matrix_perbandingan_alternatif` VALUES (5, 2, 3, 1);

-- ----------------------------
-- Table structure for matrix_perbandingan_kriteria
-- ----------------------------
DROP TABLE IF EXISTS `matrix_perbandingan_kriteria`;
CREATE TABLE `matrix_perbandingan_kriteria`  (
  `id` int(11) NOT NULL,
  `id2` int(11) NOT NULL,
  `nilai_perbandingan` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`, `id2`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of matrix_perbandingan_kriteria
-- ----------------------------
INSERT INTO `matrix_perbandingan_kriteria` VALUES (1, 2, 1);
INSERT INTO `matrix_perbandingan_kriteria` VALUES (1, 3, 1);
INSERT INTO `matrix_perbandingan_kriteria` VALUES (1, 5, 1);
INSERT INTO `matrix_perbandingan_kriteria` VALUES (2, 3, -1);
INSERT INTO `matrix_perbandingan_kriteria` VALUES (2, 5, 1);
INSERT INTO `matrix_perbandingan_kriteria` VALUES (3, 5, 1);

-- ----------------------------
-- Table structure for sales
-- ----------------------------
DROP TABLE IF EXISTS `sales`;
CREATE TABLE `sales`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sales` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `no_tim` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `penempatan` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sales
-- ----------------------------
INSERT INTO `sales` VALUES (2, 'Arman', '001', 'Bandung');
INSERT INTO `sales` VALUES (5, 'Usman', '100', 'Jakarta Barat');
INSERT INTO `sales` VALUES (6, 'Joko', '99', 'Bandung');

-- ----------------------------
-- Function structure for SPLIT_STRING
-- ----------------------------
DROP FUNCTION IF EXISTS `SPLIT_STRING`;
delimiter ;;
CREATE FUNCTION `SPLIT_STRING`(s VARCHAR(1024) , del CHAR(1) , i INT)
 RETURNS varchar(1024) CHARSET utf8mb4 COLLATE utf8mb4_general_ci
  DETERMINISTIC
BEGIN

        DECLARE n INT ;

        -- get max number of items
        SET n = LENGTH(s) - LENGTH(REPLACE(s, del, '')) + 1;

        IF i > n THEN
            RETURN NULL ;
        ELSE
            RETURN SUBSTRING_INDEX(SUBSTRING_INDEX(s, del, i) , del , -1 ) ;        
        END IF;

    END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
