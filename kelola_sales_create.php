<!DOCTYPE html>
<html lang="en">

<head>
    <title>Kelola Sales - Tambah Data</title>
    <?php include("header.php") ?>
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">
        <?php  include("header-nav.php") ?>
        <?php  include("menu.php") ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <h4>Tambah Data Sales</h4>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="kelola_sales.php">Kelola Sales</a></li>
                            <li class="breadcrumb-item active"><a href="javascript:void(0)">Tambah Data</a></li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="basic-form">
                                    <form id="form_create">
                                        <div class="form-group col-md-6">
                                            <label class="label">Nama Sales <span style="color:red;">*</span> : </label>
                                            <input type="text" name="nama_sales" class="form-control input-default " maxlength="100" required autocomplete="off">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="label">No Tim <span style="color:red;">*</span> : </label>
                                            <input type="number" name="no_tim" class="form-control input-default" required autocomplete="off">
                                        </div>

                                        <!-- 
                                        <div class="form-group col-md-6">
                                            <label class="label">Penempatan <span style="color:red;">*</span> : </label>
                                            <input type="text" name="penempatan" class="form-control input-default " maxlength="100" required autocomplete="off">
                                        </div>
                                        -->

                                    </form>
                                </div>
                                <div class="pull-right">
                                    <button type="button" id="btnCancel" class="btn btn-light">Batal</button> &nbsp; 
                                    <button type="button" id="btnSave" class="btn btn-dark">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <?php include("footer.php") ?>
        
    </div>
    <!-- Required vendors -->
    <?php include("loadscript.php") ?>

    <script>

        (function($) {
            "use strict"

            $("#btnCancel").on('click', function(){
                window.location.replace("kelola_sales.php");
            })

            $("#btnSave").on('click', function(){
                if($("#form_create").valid()){
                    $.ajax({
                        type: "POST",
                        data: $('#form_create').serialize() + "&action=kelola_sales_save",
                        url: "action.php",
                        timeout: 9000, //in milliseconds
                        success: function(data){
                            if(data.rcode == "SUCCESS"){
                                toastr.success(data.msg);
                                setTimeout(function(){
                                    window.location.replace("kelola_sales.php");
                                }, 1500);
                            } else {
                                console.log(data);
                                toastr.error(data.msg);
                            }
                        },
                        error: function(request, status, err){
                            console.log(request);
                            toastr.error(status + " " + err);
                        },
                    });
                } 
            });

        })(jQuery);

    </script>
        
    </script>
</body>

</html>