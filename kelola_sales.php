<!DOCTYPE html>
<html lang="en">

<head>
    <title>Kelola Sales </title>
    <?php include("header.php") ?>
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">
        <?php  include("header-nav.php") ?>
        <?php  include("menu.php") ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <h4>Kelola Sales</h4>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="kelola_sales.php">Kelola Sales</a></li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12" style="margin-bottom: 15px; padding-left:0px;">
                                    <a href="kelola_sales_create.php" type="button" class="btn btn-dark">Tambah Data</a>
                                </div>
                                <div class="table-responsive">
                                    <table id="example" class="table" style="min-width: 845px">
                                        <thead>
                                            <tr>
                                                <th style="width:1px">Nomor</th>
                                                <th>Nama Sales</th>
                                                <th>No Tim</th>
                                                <th>Penempatan</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $sql = "SELECT * FROM sales order by no_tim, nama_sales";
                                                $result = $conn->query($sql);
                                                $no = 0;
                                                if ($result->num_rows > 0) {
                                                    while($row = $result->fetch_assoc()) {
                                                        $no = $no+1;
                                                        echo "
                                                            <tr>
                                                                <td>".$no."</td>
                                                                <td>".$row["nama_sales"]."</td>
                                                                <td>".$row["no_tim"]."</td>
                                                                <td>".$row["penempatan"]."</td>
                                                                <td><span>
                                                                    <a href='kelola_sales_edit.php?id=".$row["id"]."' class='mr-4' data-toggle='tooltip' data-placement='top' title='Edit'><i class='fa fa-pencil color-muted'></i></a>
                                                                    <a href='#' onClick='onDelete(".$row["id"].")' data-toggle='tooltip' data-placement='top' title='Hapus'><i class='fa fa-close color-danger'></i></a>
                                                                </span></td>
                                                            </tr>
                                                        ";
                                                    }
                                                }
                                                
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <?php include("footer.php") ?>
        
    </div>

<!-- Modal -->
<div id="modalHapus" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form role="form" id="add-form">
        <div class="modal-body">
            Apakah anda yakin ingin menghapus data ini ?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-light" data-dismiss="modal">Batal</button> &nbsp; 
            <button type="button" class="btn btn-dark" onClick="onDeleteConfirm()">Hapus</button>
        </div>
      </form>
    </div>
  </div>
</div>

    <!-- Required vendors -->
    <?php include("loadscript.php") ?>
    
    <script>
        var id = "";
        (function($) {
            "use strict"
            var table = $('#example').DataTable({
                createdRow: function ( row, data, index ) {
                $(row).addClass('selected')
                } 
            });
            
            table.on('click', 'tbody tr', function() {
            var $row = table.row(this).nodes().to$();
            var hasClass = $row.hasClass('selected');
                $row.removeClass('selected')
            })
                        
            table.rows().every(function() {
                this.nodes().to$().removeClass('selected')
            });

        })(jQuery);

        function onDelete(idDelete){
            id = idDelete;
            $('#modalHapus').modal('toggle');
        }

        function onDeleteConfirm(){
            var data = new Array();
            data.push({ name: "action", value: "kelola_sales_delete" } );
            data.push({ name: "id", value: id });
            $.ajax({
                type: "POST",
                data: data,
                url: "action.php",
                timeout: 9000, //in milliseconds
                success: function(data){
                    if(data.rcode == "SUCCESS"){
                        console.log(data);
                        toastr.success(data.msg);
                        setTimeout(function(){
                            location.reload();
                        }, 1500);
                    } else {
                        console.log(data);
                        toastr.error(data.msg);
                    }
                },
                error: function(request, status, err){
                    console.log(request);
                    toastr.error(status + " " + err);
                },
            });
        }


    </script>

    
</body>

</html>