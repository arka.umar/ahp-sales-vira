<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width,initial-scale=1">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
<!-- Custom Stylesheet -->
<link href="./css/style.css" rel="stylesheet">
<link href="./css/custom.css" rel="stylesheet">

<!-- Datatable -->
<link href="./vendor/datatables/css/jquery.dataTables.min.css" rel="stylesheet">

<!-- Toastr -->
<link rel="stylesheet" href="./vendor/toastr/css/toastr.min.css">

<!-- select2 -->
<link rel="stylesheet" href="./vendor/select2/css/select2.min.css">

<?php
    include("connect.php");
?>