<!DOCTYPE html>
<html lang="en">

<head>
    <title>Perbandingan Kriteria </title>
    <?php include("header.php") ?>
</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="sk-three-bounce">
            <div class="sk-child sk-bounce1"></div>
            <div class="sk-child sk-bounce2"></div>
            <div class="sk-child sk-bounce3"></div>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">
        <?php  include("header-nav.php") ?>
        <?php  include("menu.php") ?>

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">
            <div class="container-fluid">
                <div class="row page-titles mx-0">
                    <div class="col-sm-6 p-md-0">
                        <h4>Perbandingan Kriteria</h4>
                    </div>
                    <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="perbandingan_kriteria.php">Perbandingan Kriteria</a></li>
                        </ol>
                    </div>
                </div>
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="table-responsive">
                                            <form id="form_perbandingan">
                                                <table id="tabelkriteria" class="table table-bordered" style="">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2">Pilih Kriteria Yang Lebih Peniting</th>
                                                            <th>Nilai Perbandingan</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php 
                                                            $sql = "SELECT
                                                                        a.id,
                                                                        a.nama_kriteria ,
                                                                        b.id as id2,
                                                                        b.nama_kriteria as nama_kriteria2 
                                                                    FROM
                                                                        kriteria a CROSS JOIN kriteria b 
                                                                    WHERE
                                                                        a.id != b.id and a.id < b.id
                                                                    ORDER BY
                                                                        a.id, b.id
                                                            ";
                                                            $result = $conn->query($sql);
                                                            $no = 0;
                                                            if ($result->num_rows > 0) {
                                                                while($row = $result->fetch_assoc()) {
                                                                    $no = $no+1;
                                                                    echo "
                                                                        <tr>
                                                                            <td>".$row["nama_kriteria"]."</td>
                                                                            <td>".$row["nama_kriteria2"]."</td>
                                                                            <td>
                                                                                <input type='number' name='matrix~".$row["id"]."~".$row["id2"]."' class='form-control input-default' maxlength='100' required autocomplete='off'>
                                                                            </td>
                                                                        </tr>
                                                                    ";
                                                                }
                                                            }
                                                            
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="table-responsive">
                                            <table id="tabelkriteria" class="table table-bordered" style="">
                                                <thead>
                                                    <tr>
                                                        <th style="width:1px;">Nilai</th>
                                                        <th>Keterangan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr><td>1</td><td>Kedua kriteria sama penting</td></tr>
                                                    <tr><td>3</td><td>Kriteria yang satu seidikit lebih penting</td></tr>
                                                    <tr><td>5</td><td>Kriteria yang satu seidikit lebih penting kriteria daripada yang lainnya</td></tr>
                                                    <tr><td>7</td><td>Kriteria yang satu jelas sangat penting daripada kriteria yang lainnya</td></tr>
                                                    <tr><td>9</td><td>Kriteria yang satu mutlak sangat penting daripada kriteria yang lainnya</td></tr>
                                                    <tr><td>2,4,6,8</td><td>Nilai tengah diantara dua pertimbangan yang berdekatan</td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <button type="button" id="btnSave" class="btn btn-dark">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--**********************************
            Content body end
        ***********************************-->

        <?php include("footer.php") ?>
        
    </div>

    <!-- Required vendors -->
    <?php include("loadscript.php") ?>
    
    <script>

        (function($) {
            "use strict"


            $("#btnSave").on('click', function(){
                if($("#form_perbandingan").valid()){
                    $.ajax({
                        type: "POST",
                        data: $('#form_perbandingan').serialize() + "&action=perbandingan_kriteria_save",
                        url: "action.php",
                        timeout: 9000, //in milliseconds
                        success: function(data){
                            if(data.rcode == "SUCCESS"){
                                toastr.success(data.msg);
                                setTimeout(function(){
                                    window.location.replace("perbandingan_kriteria_matrix.php");
                                }, 1500);
                            } else {
                                console.log(data);
                                toastr.error(data.msg);
                            }
                        },
                        error: function(request, status, err){
                            console.log(request);
                            toastr.error(status + " " + err);
                        },
                    });
                } 
            });

        })(jQuery);

    </script>

    
</body>

</html>